﻿using System;
using System.Threading;
using log4net;

namespace log4net_sample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Process out app.config from in this console application
            log4net.Config.XmlConfigurator.Configure();

            // Get a reference to the logger for this class
            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            // Set an end time
            var endTime = DateTime.Now.AddMinutes(10);

            long logEntryNo = 0;
            while (DateTime.Now < endTime)
            {
                // Write a log and delay, so we don't spam our log
                log.Debug("My message " + logEntryNo);
                logEntryNo++;
                Thread.Sleep(500);
            }
        }
    }
}
